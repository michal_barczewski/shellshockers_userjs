// ==UserScript==
// @name        shellshockers - clean up
// @namespace   shellshockers
// @include     http://shellshockers.uk/*
// @include     http://www.shellshockers.uk/*
// @version     7
// @grant       none
// @noframes
// ==/UserScript==
$(window).load(function () {
  console.log('removing annoying')
  add_map_toggle()
  remove_marquee()
})
//fix background not filling screen
$('body').css('background-size', 'cover');
$('body').css('background-attachment', 'fixed');
//remove duplicate background image
//todo: look for it automatically
$('.body-wrap-2').css('background-image', 'none');

//hide map
var map_cont = $('.m_html:not(:has(img))')
console.log(map_cont)
$(map_cont).hide();

//hide marquee
var text_slider = $('.textslider-box')
$(text_slider).hide()

function add_map_toggle() {
  $('iframe').each(function (i, el) {
    console.log(el);
    //var parent_div = $(el).parent();
    var is_map = /revolvermaps/.test($(el).attr('src'));
    if (is_map) {
      var toggle = $('<a/>', {
        text: 'toggle map',
        click: function () {
          $(el).toggle()
        }
      })
      $(el).parent().prepend(toggle);
      $(el).hide();
      $(map_cont).show();
    }
  })
}
//remove marquee
function remove_marquee() {
  var $marquee = $('.js-marquee')[0];
  var html = $($marquee).html();
  console.log(html)
  var new_div = $("<h2/>",{
    html: html
  })
  var container = $($marquee).closest('.special_container')
  var to_remove = $($marquee).closest('.textslider-box')
  $(container).append(new_div)
  $(to_remove).remove()
  $(text_slider).show()
}
